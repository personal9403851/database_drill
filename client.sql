USE mountblue;

CREATE TABLE Manager(
    id INT NOT NULL PRIMARY KEY,
    name VARCHAR(50),
    location VARCHAR(50)
);

INSERT INTO Manager
VALUES
    (1, 'Manager 1', 'Location of Manager 1');


CREATE TABLE staff(
    id INT NOT NULL PRIMARY KEY,
    name VARCHAR(50),
    area VARCHAR(50),
    city_Id INT,
    FOREIGN KEY (city_Id) REFERENCES City(City_Id)
);

INSERT INTO staff
VALUES
    (1, 'Staff 1', 'Erode', 2);

CREATE TABLE client(
    id INT NOT NULL PRIMARY KEY,
    name VARCHAR(50),
    location_area VARCHAR(50),
    location_city INT,
    FOREIGN KEY (location_city) REFERENCES City(City_Id),
    manager_Id INT,
    FOREIGN KEY (manager_Id) REFERENCES Manager(id),
    contract_Id INT NOT NULL,
    estimated_cost FLOAT,
    completion_date DATE,
    staff_Id INT,
    FOREIGN KEY (staff_Id) REFERENCES staff(id)
);

INSERT INTO client
VALUES
    (1, 'Client 1', 'Kormangala', 2, 1, 1, 5436.74, '2024-02-22', 1);