CREATE DATABASE mountblue;

CREATE TABLE Publisher(
    Publisher_Id INT NOT NULL PRIMARY KEY,
    Publisher_Name VARCHAR(50)
);

INSERT INTO Publisher(Publisher_Id, Publisher_Name)
VALUES (101, 'George RR Martin');

CREATE TABLE City(
    City_Id INT NOT NULL PRIMARY KEY,
    City_Name VARCHAR(50)
);

INSERT INTO City(City_Id, City_Name)
VALUES 
    (1, 'Bangalore'),
    (2, 'Hyderabad');

CREATE TABLE Branch(
	BranchId INT NOT NULL PRIMARY KEY,
    Branch_Area VARCHAR(100),
    Branch_City INT,
    FOREIGN KEY (Branch_City) REFERENCES City(City_Id),
    ISBN INT,
    Title VARCHAR(50),
    Author VARCHAR(50),
    PublisherId INT,
    FOREIGN KEY (PublisherId) REFERENCES Publisher(Publisher_Id),
    Num_copies INT
);

INSERT INTO Branch(BranchId, Branch_Area, Branch_City, ISBN, Title, Author, PublisherId, Num_copies)
VALUES
 (1, 'Kormangala', 1, 15394, 'Winterfell', 'Ned Stark', 101, 10),
 (2, 'HSR Layout', 1, 12456, 'DragonStone', 'Daenerys Targaryen', 101, 11),
 (3, 'Whitefield', 2, 64648, 'Harrenhal', 'Lucas Harroway', 101, 12),
 (4, 'Brookefield', 1, 79453, 'Highgarden', 'Mace Tyrell', 101, 13),
 (5, 'BTM Layout', 1, 87944, 'Casterly Rock', 'Tywin Lannister', 101, 14),
 (6, 'Arekere', 2, 32458, 'Castle of Zafra', 'Emmon Frey', 101, 15);