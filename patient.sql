USE mountblue;

CREATE TABLE secretary(
    id INT PRIMARY KEY,
    name VARCHAR(50)
);

INSERT INTO secretary
VALUES
    (1, "Secretary 1");

CREATE TABLE patient(
    id INT NOT NULL PRIMARY KEY,
    name VARCHAR(50),
    dob DATE,
    address_area VARCHAR(50),
    address_city INT,
    FOREIGN KEY (address_city) REFERENCES City(City_Id),
    prescription_number INT NOT NULL,
    drug VARCHAR(50),
    date DATE,
    dosage text,
    doctor VARCHAR(50),
    secretary_Id INT,
    FOREIGN KEY (secretary_Id) REFERENCES secretary(id) 
);

INSERT INTO patient
VALUES
    (1, "Patient 1", '2001-11-16', 'Kormangala', 1, 123, 'Anesthesia', '2024-01-23', '3-times per day', "Doctor 1", 1);