USE mountblue;

CREATE TABLE doctor(
    id INT NOT NULL PRIMARY KEY,
    name VARCHAR(50),
    secretary_Id INT,
    FOREIGN KEY (secretary_Id) REFERENCES secretary(id),
    patient_id INT,
    FOREIGN KEY (patient_id) REFERENCES patient(id)
);

INSERT INTO doctor
VALUES
    (1, "Doctor 1", 1, 1);